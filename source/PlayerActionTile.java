/**
 * PlayerActionTile.java
 * class that has methods to alter the player on the board
 * inherits from ActionTile
 * @author Sam Harry, Harry Stradling
 * Editted 22/11/2020 by Charlie Bramble
 */
public class PlayerActionTile extends ActionTile {

    /**
     * Constructor for a new PlayerActionTile
     * @param tileType TileType, the type of the tile
     */
    public PlayerActionTile(TileType tileType) {
        super(tileType);
    }

    /**
     * Use the tile's ability - it is different based upon its type
     * @param player Player, the player that the tile is being activated on
     */
    public void activateTile(Player player) {
    	if (this.getTileType() == TileType.DOUBLEMOVE) {
    		this.backtrackPlayer(player);
    	} else if (this.getTileType() == TileType.BACKTRACK) {
    		this.doubleMove();
    	}
    }

    /**=============================================SOMEONE FILL THIS OUT FOR ME=================================================================
     * The backtrack tile ability
     * @param player
     */
    private void backtrackPlayer(Player player) {
    	player.move(player.getPreviousPositions().get(2)); //Temporary - needs updating to allow chance of 1 move
    }

    /** =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
     * The double move tile ability
     */
    private void doubleMove() {
    }

}