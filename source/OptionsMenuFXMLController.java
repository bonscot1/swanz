/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Slider;


/**
 *
 * @author Harry Stradling, Sam Harry
 */
public class OptionsMenuFXMLController extends SwanzGUI {


    @FXML
    Slider volumeSlider;
    @FXML
    private void backButtonHandler(ActionEvent event) throws Exception {
        mainMenu();
    };

    public void initialize() {

        volumeSlider.setValue(mediaPlayer.getVolume() * 100);
        volumeSlider.valueProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable observable) {
                mediaPlayer.setVolume(volumeSlider.getValue() / 100);
            }
            });

    }
}







