import java.util.LinkedList;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;

/**
 * Bag.java
 * This is the container that holds all of the tiles that are not currently
 * on the board but are also in play.
 * It is the programatic version of a silk bag in a board game/
 * 
 * @author Abby Stevenson
 * @author Meghan Del Rosario
 * @version 1.2
 * Updated 02/12/2020
 * Updated by Charlie Bramble
 */

public class Bag implements Serializable {

    //The current game
    private Game game;
    //The tiles stored
    private LinkedList<Tile> tiles;

    /**
     * New bag constructor
     * @param tilesArray Tile[], the array of tiles that are in the bag
     * @param game Game, the game that this bag is currently within
     */
    public Bag (Tile[] tilesArray, Game game) {
        this.tiles = new LinkedList<Tile>(Arrays.asList(tilesArray));
        this.game = game;
        this.randomise();
    }
    
    /**
     * Randomise method that randomises the order of the tile linked list and the orientation values of the tiles
     *
     */
    public void randomise() {Collections.shuffle(this.tiles);}
    
    /**
     * Insert a tile into the bag for later use and re randomised
     * @param newTile Tile, the tile to be inserted
     */
    public void insert(Tile newTile){
        this.tiles.add(newTile);
        this.randomise();
    }
    
    /**
     * Inserting a tile without randomising the bag
     * @param newTile Tile, the tile to be inserted
     */
    public void insertNoRand(Tile newTile){
        this.tiles.add(newTile);
    }

    /**
     * Takes the tile at the from of the list and removes it (and returns it)
     * @return takenTile Tile, the tile at the front of the list
     */
    public Tile take() {
        Tile takenTile = this.tiles.removeFirst();
        return takenTile;
    }

    /**
     * Get the game that the bag is within
     * @return Game game, the game
     */
    public Game getGame() {
        return this.game;
    }
}

