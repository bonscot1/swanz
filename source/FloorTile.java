import java.util.concurrent.ThreadLocalRandom;

/**
 * FloorTile.java
 * stores information on the FloorTile objects
 * class that inherits from the Tile class
 * @author Sam Harry
 * @author Harry Stradling
 * Created 13/11/20
 * Edited 21/11/2020 by Dmitriy
 * Edited 02/12/2020 by Charlie Bramble
 * Edited 04/12/2020 by Dmitriy
 */
public class FloorTile extends Tile {

    private int orientation;
    private boolean isBurning;
    private boolean isFrozen;
    private boolean isFixed;
    private boolean[] sides;
    /*
    sides array:
    0 = LEFT
    1 = TOP
    2 = RIGHT
    3 = BOTTOM
    */

    /**
     * FloorTile constructor
     * @param tileType TileType, the type that the tile is
     */
    public FloorTile(TileType tileType) {
        super(tileType);
        this.orientation = 0;//0 is the default orientation
        switch (tileType) {
            case STRAIGHT:
                this.sides = new boolean[]{true, false, true, false};
                break;
            case CORNER:
                this.sides = new boolean[]{false, true, true, false};
                break;
            case TSHAPED:
                this.sides = new boolean[]{false, true, true, true};
                break;
            case GOAL:
                this.sides = new boolean[]{true, true, true, true};
                break;
            default:
                break;
        }
        //Random to determine how many times the tile is rotated on creation
        //it is rotated 0 to 3 times to give chance to all possible orientations
        //the function is none inclusive so 4 is used instead of 3
        int randomNum = ThreadLocalRandom.current().nextInt(0, 4);
        for (int x = 0; x <= randomNum; x++) {
            rotateL();
        }
    }

    /**
     * Rotate the tile 90 degrees anticlockwise and update its sides
     */
    public void rotateL() {
        this.orientation += 1;
        if (orientation == 4) {
            orientation = 0;
        }
        boolean temp = sides[3];

        for (int j = 2; j >= 0; j--) {
            sides[j + 1] = sides[j];
        }
        sides[0] = temp;
    }

    /**
     * gets orientation
     */
    public int getOrientation() {
        return orientation;
    }

    /**
     * Toggle whether tile is on fire or not
     */
    public void toggleFire() {
        isBurning = !isBurning;
    }

    /**
     * Toggle whether the tile is frozen in place or not
     */
    public void toggleFreeze() {
        isFrozen = !isFrozen;
    }

    /**
     * Toggle whether the tile is fixed or not
     */
    public void toggleFixed() {
        isFixed = !isFixed;
    }

    /**
     * Get whether the tile is frozen or not
     * @return boolean, whether tile is frozen
     */
    public boolean isFrozen() {
        return this.isFrozen;
    }

    /**
     * Get whether the tile is burning or not
     * @return boolean, whether the tile is burning or not
     */
    public boolean isBurning() {
        return this.isBurning;
    }

    /**
     * Whether this tile is set to be unable to move
     * @return boolean, whether this tile is a fixed tile
     */
    public boolean isFixed() {
        return this.isFixed;
    }

    /**
     * @param side one of the 4 sides of the tile which we want to check
     * @return true if the demanded side is open, false otherwise
     */
    public boolean edgeOpen(int side) {
        return sides[side];
    }

    public boolean[] getSides() {
        return sides;
    }
}