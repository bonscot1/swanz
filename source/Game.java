import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
/**
 * Game.java
 * Updated 22/11/2020 by Charlie Bramble
 * @author Meghan Del Rosario
 * @author Abby Stevenson
 * @version 1.1
 */

public class Game implements Serializable {

    private Board board;
    private Player[] players;
    private Level levelInfo;
    private Bag bag;
    private int playerTurn;
    private String gameName;

    /**
     * The constructor for a new game
     * @param players PlayerProfile[] an array of all the profiles selected for the game
     * @param levelInfo Level, the info for the level
     */
    public Game (PlayerProfile[] players, Level levelInfo) {
        this.playerTurn = 0;
        this.players = createPlayers(players);
        this.levelInfo = levelInfo;
        this.bag = new Bag(levelInfo.getBagTiles(), this);
        this.board = new Board(levelInfo, this);
    }

    /**
     * The winner of the game
     * @param p Player, the winner of the game
     */
    public void winner (Player p) {
    }

    /**
     * Get a player based on their turn number / player number
     * @param playerNumber int, the player number wanted
     * @return Player, the player object of that player number
     */
    public Player getPlayer (int playerNumber) {
        return players[playerNumber];
    }

    /**
     * Change a players position on the board
     * @param playerNumber int, the player number that is wanted
     * @param position Point, the position that the player will be moved
     */
    public void setPlayerPosition (int playerNumber, Point position) {
        players[playerNumber].move(position);
    }

    /**
     * Create the players for the game based off of their profiles
     * @param players PlayerProfile[], the profiles of the players in the game
     * @return Player[] the players to be used in the game.
     */
    public Player[] createPlayers (PlayerProfile[] players) {
        Player[] chosenPlayers = new Player[players.length];
        for (int x = 0; x < players.length; x++) {
            chosenPlayers[x] = new Player(players[x], this);
        }
        return chosenPlayers;
    }

    public String getGameName() {
        return this.gameName;
    }

    public void setGameName(String gameName) {
        this.gameName = gameName;
    }

    public Level getLevel() {
        return this.levelInfo;
    }

    public int getPlayerTurn() {
        return this.playerTurn;
    }

    public Board getBoard() {
        return this.board;
    }

    public Bag getBag() {
        return this.bag;
    }

    public Player[] getPlayers() {
        return this.players;
    }

    /**
     * Saves the current game state to a serialised file
     */
    public void saveGame() {
        String folderLocation = "saves/" + this.gameName;
        FileIntegration.serialise(this, folderLocation);
    }

    /**
     * Loads a game that was previously saved to file
     * @param gameName String, the game name of the saved game
     * @return Game, the instant of the game
     */
    public static Game loadGame(String gameName) {
        return FileIntegration.deserialise("saves/" + gameName);
    }

    /**
     * Gets all save games on file
     * @return ArrayList<Game>, array list of save games
     */
    public static ArrayList<Game> getAllGames() {
        return FileIntegration.getAll("saves/");
    }
}
