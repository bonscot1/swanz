
import javafx.event.ActionEvent;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;



import java.awt.*;
import java.util.ArrayList;



public class GameStageFXMLController {
    final int MAX_X;
    final int MAX_Y;

    @FXML
    private GridPane board = new GridPane();
    @FXML
    private Label tileDrawnLabel = new Label();
    @FXML
    BorderPane drawnCardBP = new BorderPane();

    private int type[] = new int[100];
    private int count = 0;
    private int currentPlayer;

    private Point[] posArray;
    private ArrayList<ImageView> playerIVArray = new ArrayList<ImageView>();
    private Game gameInstance;

    //private StackPane playerSP = new StackPane();
    public GameStageFXMLController(){
        this.gameInstance = SwanzGUI.getGameInstance();
        this.MAX_X = gameInstance.getBoard().getGameBoard().length;
        this.MAX_Y = gameInstance.getBoard().getGameBoard()[0].length;
        this.posArray = gameInstance.getLevel().getPlayerStartingLocations();
        for(int i = 0; i < 4; i++) {
            ImageView playerIV = new ImageView();
            playerIV.setImage(new Image(getClass().getResource("media/image/Player_1.png").toString()));
            this.playerIVArray.add(playerIV);
        }
    }




    @FXML
    private void drawTileHandler(ActionEvent event){
        Tile t =drawTile();
    }
    // nto finished
    private void turn(){
        this.currentPlayer = gameInstance.getPlayerTurn();
        //updateEffects();
        Tile drawnTile = drawTile();
        if (drawnTile instanceof FloorTile){

        }else if (drawnTile instanceof ActionTile){
            if (gameInstance.getPlayer(currentPlayer).getInventory().size() == 0){

            }

        }

    }
    @FXML
    private Tile drawTile(){
        gameInstance.getBag().randomise();
        Tile drawnTile = gameInstance.getBag().take();
        ImageView drawnTileIV = setIV(drawnTile);
        drawnCardBP.setCenter(drawnTileIV);
        tileDrawnLabel.setText("You Drew a " + drawnTile.getTileType().toString() + " tile");
        return drawnTile;

    }
    // puts players on board
    private void populatePlayers(){
        for (int i = 0; i < posArray.length;i++){
            ((StackPane) getNodeFromGridPane(board, posArray[i])).getChildren().add(playerIVArray.get(i));
        }
    }

    // gets a stackpane from index on gridpane
    private Node getNodeFromGridPane(GridPane board, Point pos) {
        for (Node node : board.getChildren()) {
            if (board.getColumnIndex(node) == pos.getY() && GridPane.getRowIndex(node) == pos.getX()) {
                return node;
            }
        }
        return null;
    }

    private Boolean move(Point p){
        System.out.println("Click on an adjacent index to move");
        if (gameInstance.getBoard().canMove(posArray[currentPlayer], p)){
            movePlayer(p);
            return true;
        } else {return false;}
    }

    private void movePlayer(Point p){
        ((StackPane) getNodeFromGridPane(board, posArray[currentPlayer])).getChildren().remove(playerIVArray.get(currentPlayer));
        ((StackPane) getNodeFromGridPane(board, p)).getChildren().add(playerIVArray.get(currentPlayer));
        gameInstance.getPlayer(currentPlayer).move(p);
        posArray[currentPlayer] = p;


    }
    private ImageView setIV(Tile t) {
        ImageView tIV = new ImageView();
        if (t.getTileType() == TileType.STRAIGHT){
            tIV.setImage(new Image(getClass().getResource("media/image/StraightTile.png").toString()));
            return tIV;

        } else if (t.getTileType().equals(TileType.CORNER)){
            tIV.setImage(new Image(getClass().getResource("media/image/CornerTile.png").toString()));
            return tIV;

        } else if (t.getTileType().equals(TileType.TSHAPED)){
            tIV.setImage(new Image(getClass().getResource("media/image/TTile.png").toString()));
            return tIV;
        }
        else if (t.getTileType().equals(TileType.GOAL)){
            tIV.setImage(new Image(getClass().getResource("media/image/GoalTile.png").toString()));
            return tIV;
        }else if (t.getTileType().equals(TileType.HORDE)){
            tIV.setImage(new Image(getClass().getResource("media/image/GoalTile.png").toString()));
            return tIV;
        }else if (t.getTileType().equals(TileType.ICE)){
            tIV.setImage(new Image(getClass().getResource("media/image/GoalTile.png").toString()));
            return tIV;
        }
        return null ;// exception?

    }



    private void populateBoard(){
        board.setHgap(-5);
        board.setVgap(-5);
        board.setAlignment(Pos.CENTER);
        for (int i = 0; i < MAX_X; i++){
            ColumnConstraints column = new ColumnConstraints(100);
            board.getColumnConstraints().add(column);
        }
        for (int i = 0; i < MAX_Y; i++){
            RowConstraints row = new RowConstraints(100);
            board.getRowConstraints().add(row);
        }

        for (int x = 0;x < MAX_X; x++){
            for (int y = 0;y < MAX_Y; y++){
                FloorTile t = gameInstance.getBoard().getTileFromIndex(x,y);
                ImageView tileIV = new ImageView();
                StackPane playerSP = new StackPane();
                tileIV = setIV(t);


                tileIV.setFitHeight(96);
                tileIV.setFitWidth(96);
                int finalX = x;
                int finalY = y;

                tileIV.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                    System.out.printf("%d%s%n", finalX, finalY);
                    Point newPos = new Point(finalX, finalY);
                    //System.out.println(gameInstance.getBoard().canMove(posArray[currentPlayer], newPos));
                    //boolean[] players = gameInstance.getBoard().getTileFromIndex((int)posArray[currentPlayer].getX(), (int)posArray[currentPlayer].getY()).getSides();
                    //boolean[] players1 = gameInstance.getBoard().getTileFromIndex( finalX, finalY).getSides();

                    //System.out.println(players);
                    System.out.println(gameInstance.getBoard().canMove(posArray[currentPlayer], newPos));
                    //move(newPos);

                    event.consume();
                });

                playerSP.maxHeight(96.0);
                playerSP.maxWidth(96.0);
                //Label l = new Label(Integer.toString(count));
                //l.setTextFill(Color.web("#FFF"));
                playerSP.getChildren().addAll(tileIV);

                System.out.println(count);
                board.addRow(x, playerSP);
                count++;
            }
        }
    }



    @FXML
    private void initialize(){
        populateBoard();
        populatePlayers();
        currentPlayer = 1;

















    }
}
