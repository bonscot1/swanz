/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.io.Serializable;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 *
 * @author Harry Stradling, Sam Harry
 * Controller Class for the new game FXML
 * Controls all the functionality for the start new game scene of the program
 */
public class NewGameMenuFXMLController extends SwanzGUI implements Serializable{

    PlayerProfile player1;
    PlayerProfile player2;
    PlayerProfile player3;
    PlayerProfile player4;

    String newGameName;
    String selectedLevel;

    private ArrayList<PlayerProfile> playerList = PlayerProfile.playerList();
    ArrayList<String> levelList = new ArrayList<>();
    ObservableList<PlayerProfile> selectedPlayerList = FXCollections.observableArrayList();

    @FXML
    ComboBox<String> player1ComboBox = new ComboBox<>();
    @FXML
    ComboBox<String> player2ComboBox = new ComboBox<>();
    @FXML
    ComboBox<String> player3ComboBox = new ComboBox<>();
    @FXML
    ComboBox<String> player4ComboBox = new ComboBox<>();
    @FXML
    ChoiceBox<String> levelDropDown = new ChoiceBox<>();
    @FXML
    TextField newGamePrompt = new TextField();
    @FXML
    Button startNewGameButton = new Button();

    /**
     * Handler for the back button that takes the user to the main menu
     * @param event
     * @throws Exception
     */
    @FXML
    private void backButtonHandler(ActionEvent event) throws Exception {
        mainMenu();
    }

    /**
     * handler for the ComboBoxes that sets the value of them to the value displayed in them
     * @param event
     * @throws Exception
     */
    @FXML
    private void playerComboBoxHandler(ActionEvent event) throws Exception {
        player1ComboBox.setValue(player1ComboBox.getValue());
        player2ComboBox.setValue(player2ComboBox.getValue());
        player3ComboBox.setValue(player3ComboBox.getValue());
        player4ComboBox.setValue(player4ComboBox.getValue());
    }

    /**
     * handler for the level ChoiceBox that sets the value of the drop down list to the value it displays
     * @throws Exception
     */
    @FXML void setLevelDropDownHandler() throws Exception {
        levelDropDown.setValue(levelDropDown.getValue());
    }

    /**
     * handler for the start new game button that controls most of the functionality for the screen
     * performs multiple error checks to see if the user is attempting to start a game with invalid information
     * e.g incorrect player parameters, no level selected or duplicates for new players and existing players
     * @param event
     * @throws Exception
     */
    @FXML
    private void setStartNewGameButtonHandler(ActionEvent event) throws Exception {

        populateAndNewPlayers();

        //case where no players are selected
        if (player1ComboBox.getValue() == null) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
            return;
        }

        //case of 1 player trying to be used
        else if (selectedPlayerList.size() == 1) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
            return;
        }

        //if player 2 is not empty and player 1 is
        else if (player2ComboBox.getValue() != null && (player1ComboBox.getValue() == null)) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
            return;
        }

        //if player 3 isn't empty and either player 1 or 2 are empty
        else if (player3ComboBox.getValue() != null && (player1ComboBox.getValue() == null || player2ComboBox.getValue()
                == null)) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
            return;
        }

        //if player 4 isn't empty and either player 1, 2 or 3 are empty
        else if (player4ComboBox.getValue() != null && (player1ComboBox.getValue() == null || player2ComboBox.getValue()
                == null || player3ComboBox.getValue() == null)) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
            return;
        }

        //if 2 players are selected and player 1 is the same as player 2
        else if (selectedPlayerList.size() == 2 && (selectedPlayerList.get(0) == selectedPlayerList.get(1))) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
        }

        //if 3 players are selected and player 1 is the same as player 2, or player 1 is the same as player 2
        else if (selectedPlayerList.size() == 3 && (selectedPlayerList.get(0) == selectedPlayerList.get(1)) ||
                (selectedPlayerList.get(0) ==
                        playerList.get(2)) || (playerList.get(1) == playerList.get(2))) {
            selectedPlayersError();
            clearComboAndSelectedPlayers();
        }
        //the case of 4 players trying to be used but containing duplicates
        else if (player1ComboBox.getValue() != null && player2ComboBox.getValue() != null && player3ComboBox.getValue() !=
                null && player4ComboBox.getValue() != null) {
            if (player1ComboBox.getValue() == player2ComboBox.getValue() ||
                    player1ComboBox.getValue() == player3ComboBox.getValue() ||
                    player1ComboBox.getValue() == player4ComboBox.getValue() ||
                    player2ComboBox.getValue() == player3ComboBox.getValue() ||
                    player2ComboBox.getValue() == player4ComboBox.getValue() ||
                    player3ComboBox.getValue() == player4ComboBox.getValue()) {
                selectedPlayersError();
                clearComboAndSelectedPlayers();
                return;
            }
        }

        //branch to create a new game
        else {
            selectedLevel = levelDropDown.getValue();
            newGameName = newGamePrompt.getText();
            //check for whether the level has not been selected
            if (selectedLevel == null) {
                levelSelectedError();
                clearComboAndSelectedPlayers();
                selectedLevel = null;
                levelDropDown.setValue(null);
                return;
            }
            //checks to see if the game name has not been entered
            else if (newGameName.isEmpty()) {
                gameNameError();
                clearComboAndSelectedPlayers();
                newGameName = null;
                newGamePrompt.clear();
                return;
            }

            //checks to see if this game name already exists
            for (int i = 0; i < Game.getAllGames().size(); i++) {
                if (Game.getAllGames().get(i).getGameName() == newGameName) {
                    gameNameError();
                    clearComboAndSelectedPlayers();
                    newGameName = null;
                    newGamePrompt.clear();
                    return;
                }
            }

            //creates an array from the observable arraylist
            PlayerProfile[] playerArray = new PlayerProfile[selectedPlayerList.size()];
            playerArray = selectedPlayerList.toArray(playerArray);
            ArrayList<Level> levelList = Level.getAllLevels();
            //creates a new game from a level if the level name exists in the drop down list

            for(int i = 0; i < levelList.size(); i++) {
                if (levelList.get(i).getLevelName().equals(selectedLevel)) {
                    Game newGame = new Game(playerArray, levelList.get(i));
                    newGame.setGameName(newGameName);
                    newGame.saveGame();
                    //sets the game instance and calls the game stage
                    setGameInstance(newGame);
                    gameStage();
                }
            }
        }
    }

    /**
     * method to populate the selectedPlayerList with the players of the drop downlists
     * also creates new players if they do not exist in the profiles folder
     */
    public void populateAndNewPlayers() {
        //for loop to populate the selected players from the combo boxes
        for (int i = 0; i < playerList.size() + 1; i++) {
            if (player1ComboBox.getValue() == playerList.get(i).getProfileName()) {
                player1 = playerList.get(i);
                selectedPlayerList.add(player1);
                break;
            } else if (i == playerList.size() && player1ComboBox.getValue() != null) {
                PlayerProfile newPlayerProfile = new PlayerProfile(player1ComboBox.getValue());
                FileIntegration.serialise(newPlayerProfile, "profiles/");
                selectedPlayerList.add(newPlayerProfile);
                break;
            }
            else if (player1ComboBox.getValue() == null) {
                break;
            }
        }
        for (int i = 0; i < playerList.size() + 1; i++) {
            if (player2ComboBox.getValue() == playerList.get(i).getProfileName()) {
                player2 = playerList.get(i);
                selectedPlayerList.add(player2);
                break;
            }
            else if (i == playerList.size() && player2ComboBox.getValue() != null) {
                PlayerProfile newPlayerProfile = new PlayerProfile(player2ComboBox.getValue());
                FileIntegration.serialise(newPlayerProfile, "profiles/");
                selectedPlayerList.add(newPlayerProfile);
                break;
            }
            else if (player2ComboBox.getValue() == null) {
                break;
            }
        }
        for (int i = 0; i < playerList.size() + 1; i++) {
            if (player3ComboBox.getValue() == playerList.get(i).getProfileName()) {
                player3 = playerList.get(i);
                selectedPlayerList.add(player3);
                break;
            }
            else if (i == playerList.size() && player3ComboBox.getValue() != null) {
                PlayerProfile newPlayerProfile = new PlayerProfile(player3ComboBox.getValue());
                FileIntegration.serialise(newPlayerProfile, "profiles/");
                selectedPlayerList.add(newPlayerProfile);
                break;
            }
            else if (player3ComboBox.getValue() == null) {
                break;
            }
        }
        for (int i = 0; i < playerList.size() + 1; i++) {
            if (player4ComboBox.getValue() == playerList.get(i).getProfileName()) {
                player4 = playerList.get(i);
                selectedPlayerList.add(player4);
                break;
            }
            else if (i == playerList.size() && player4ComboBox.getValue() != null) {
                PlayerProfile newPlayerProfile = new PlayerProfile(player4ComboBox.getValue());
                FileIntegration.serialise(newPlayerProfile, "profiles/");
                selectedPlayerList.add(newPlayerProfile);
                break;
            }
            else if (player4ComboBox.getValue() == null) {
                break;
            }
        }
    }

    /**
     * method to clear the combo boxes and the selected players array, in the event that an error is thrown
     */
    public void clearComboAndSelectedPlayers() {
        player1ComboBox.setValue(null);
        player2ComboBox.setValue(null);
        player3ComboBox.setValue(null);
        player4ComboBox.setValue(null);
        selectedPlayerList.clear();
        levelDropDown.setValue(null);
    }

    /**
     * method to throw an alert box if the selected players are incorrect
     */
    public void selectedPlayersError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText("Invalid player selection");
        alert.setContentText("You entered an invalid collection of players"
                + "\n" + "\n" + "Please re-select players");
        alert.showAndWait();
    }

    /**
     * method to throw an alert box if the game name has not been entered
     */
    public void gameNameError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText("Incorrect game name");
        alert.setContentText("You tried to start a game with no name entered, or a name that exists already"
                + "\n" + "\n" + "Please re-enter with a game name");
        alert.showAndWait();
    }

    /**
     * method to throw an alert box if the level is not selected
     */
    public void levelSelectedError() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText("Incorrect level selected");
        alert.setContentText("You tried to start a game with no level selected"
                + "\n" + "\n" + "Please re-enter with a level selected from the drop down");
        alert.showAndWait();
    }

    /**
     * method that initializes the containers at the start of the scene
     */
    public void initialize () {
        for (int i = 0; i < playerList.size(); i++) {
            player1ComboBox.getItems().add(playerList.get(i).getProfileName());
            player2ComboBox.getItems().add(playerList.get(i).getProfileName());
            player3ComboBox.getItems().add(playerList.get(i).getProfileName());
            player4ComboBox.getItems().add(playerList.get(i).getProfileName());
        }

        for (int i = 0; i < Level.getAllLevels().size(); i++) {
            levelList.add(Level.getAllLevels().get(i).getLevelName());
            levelDropDown.getItems().add(levelList.get(i));
        }
    }
}

