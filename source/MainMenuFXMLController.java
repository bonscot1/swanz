/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import javafx.event.ActionEvent;
import javafx.fxml.FXML;


/**
 *
 * @author Harry
 */
public class MainMenuFXMLController extends SwanzGUI {


    @FXML
    private void newGameButtonHandler(ActionEvent event) throws Exception {
        newGameMenu();
    };
    @FXML
    private void leaderboardButtonHandler(ActionEvent event) throws Exception {
        leaderboardMenu();
    };
    @FXML
    private void loadGameButtonHandler(ActionEvent event) throws Exception {
        loadGameMenu();
    };
    @FXML
    private void optionsButtonHandler(ActionEvent event) throws Exception {
        optionsMenu();
    };
    @FXML
    private void exitButtonHandler(ActionEvent event) throws Exception {
        System.exit(0);
    };

    public void initialize() {


    }






}
