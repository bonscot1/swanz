/**
 * BoardActionTile.java
 * class that inherits from the ActionTile and the Tile class
 * @author Sam Harry, Harry Stradling
 * @version 1.1
 * Created 13/11/20
 * Edited 02/12/2020 by Charlie Bramble
 */
public class BoardActionTile extends ActionTile {

    private Tile[] affectedTiles;

    /**
     * Constructor for creating a board action tile
     * @param tileType TileType enum, the type of the tile
     */
    public BoardActionTile(TileType tileType) {
        super(tileType);
    }

    /**
     * =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
     * Pretty sure this should be in the Effect.java not here
     */
    public void decTimer() {
    	//no idea how to implement this, sorry :(
    }

    /**
     * =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
     * again, pretty sure this should be in the effect file and then an effect be stored in this
     * @return
     */
    public Tile[] getAffectedTiles() {
        return this.affectedTiles;
    }

    public void setAffectedTiles() {
    	//same here
    }
}