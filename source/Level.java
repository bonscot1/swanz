import java.awt.Point;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Level.java
 * A class designed to read the level files and construct a class with all the information needed for the board
 * @author Alex Gregory
 */
public class Level implements Serializable {

    private String levelName;
    private int width;
    private int height;
    private Point[] fixedTileLocations;
    private FloorTile[] fixedTiles;
    private Tile[] bagTiles;
    private Point[] playerStartingLocations;

    /**
     * Create the level class, which contains all the information needed for the board to be instantiated
     * @param Scanner classInfo - the information needed to build the class in scanner form
     * @param String levelName - a string containing the name of the string
     */
    public Level (Scanner classInfo, String levelName) {
        this.levelName = levelName;
        ArrayList<FloorTile> fixedTiles = new ArrayList<FloorTile>();
        ArrayList<Point> fixedTileLocations = new ArrayList<Point>();
        ArrayList<Tile> bagTiles = new ArrayList<Tile>();
        Point[] playerStartingLocations = new Point[4];

        Scanner sc = newScannerWithDelim(classInfo);
        setHeightAndWidth(sc);

        sc = newScannerWithDelim(classInfo);
        int fixedTileNumber = sc.nextInt();

        for (int i = 0; i < fixedTileNumber; i++) {
            sc = newScannerWithDelim(classInfo);
            fixedTileLocations.add(createLocation(sc));
            fixedTiles.add(createFloorTileFromFile(sc));
        }

        for (int i = 0; i < 4; i++) {
            sc = newScannerWithDelim(classInfo);
            playerStartingLocations[i] = createLocation(sc);
        }

        sc = newScannerWithDelim(classInfo);
        addMultiFloorTiles(TileType.STRAIGHT, sc.nextInt(), bagTiles);
        addMultiFloorTiles(TileType.CORNER, sc.nextInt(), bagTiles);
        addMultiFloorTiles(TileType.TSHAPED, sc.nextInt(), bagTiles);

        sc = newScannerWithDelim(classInfo);
        addMultiActionTiles(TileType.ICE, sc.nextInt(), bagTiles);
        addMultiActionTiles(TileType.HORDE, sc.nextInt(), bagTiles);
        addMultiActionTiles(TileType.DOUBLEMOVE, sc.nextInt(), bagTiles);
        addMultiActionTiles(TileType.BACKTRACK, sc.nextInt(), bagTiles);

        this.fixedTiles = fixedTiles.toArray(new FloorTile[fixedTiles.size()]);
        this.fixedTileLocations = fixedTileLocations.toArray(new Point[fixedTileLocations.size()]);
        this.bagTiles = bagTiles.toArray(new Tile[bagTiles.size()]);
        this.playerStartingLocations = playerStartingLocations;
    }

    /**
     * Sets the height and width of the level
     * @param Scanner sc - class info scanner
     */
    private void setHeightAndWidth(Scanner sc){
        this.width = sc.nextInt();
        this.height = sc.nextInt();
    }

    /**
     * Create a Point objcet from an x and y in the scanner
     * @param Scanner sc - class info scanner
     * @return a Point object with the x and y from the scanner
     */
    private Point createLocation(Scanner sc){
        int x = sc.nextInt();
        int y = sc.nextInt();
        return new Point(x, y);
    }

    /**
     * Create a floor tile from the information in the scanner
     * 
     * @param Scanner sc - class info scanner
     * @return a floor tile object created from the information in the scanner
     * @throws InstantiationException
     */
    private FloorTile createFloorTileFromFile(Scanner sc) {
        String tileType = sc.next();
        TileType tileTypeEnum = TileType.STRAIGHT;
        switch (tileType) {
            case "straight":
                tileTypeEnum = TileType.STRAIGHT;
                break;
            case "corner":
                tileTypeEnum = TileType.CORNER;
                break;
            case "tshaped":
                tileTypeEnum = TileType.TSHAPED;
                break;
            case "goal":
                tileTypeEnum = TileType.GOAL;
                break;
        }
        if (null == tileTypeEnum) {
            //throw new InstantiationException("Level file's format incorrect!");
            // WE NEED TO START THROWING EXCEPTIONS - EG HERE
        }
        int tileRotation = sc.nextInt();
        return new FloorTile(tileTypeEnum);
    }

    /**
     * Create an amount of floor tiles with a given type, and append them into the tiles arraylist that's parsed
     * @param TileType tileType - the ENUM type of the floor tile
     * @param int amountOfTiles - the amount of tiles to be created
     * @param ArrayList<Tile> tiles - the array list that will contain the tiles
     */
    private void addMultiFloorTiles(TileType tileType, int amountOfTiles, ArrayList<Tile> tiles){
        for (int i = 0; i < amountOfTiles; i++){
            tiles.add(new FloorTile(tileType));
        }
    }

    /**
     * Create an amount of action tiles with a given type, and append them into the tiles arraylist that's parsed
     * @param TileType tileType - the ENUM type of the action tile
     * @param int amountOfTiles - the amount of tiles to be created
     * @param ArrayList<Tile> tiles - the array list that will contain the tiles
     */
    private void addMultiActionTiles(TileType tileType, int amountOfTiles, ArrayList<Tile> tiles){
        for (int i = 0; i < amountOfTiles; i++){
            tiles.add(new ActionTile(tileType));
        }
    }

    /**
     * Get the next line from the parsed scanner and return that as a scanner with the delimiter set as ','
     * @param Scanner classInfo - the scanner parsed
     * @return a scanner containing a single line from the classinfo with the delimiter set
     */
    private Scanner newScannerWithDelim(Scanner classInfo){
        Scanner sc = new Scanner(classInfo.nextLine());
        sc.useDelimiter(",");
        return sc;
    }

    /**
     * Returns the level name
     * @return level name
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * Returns the level width
     * @return level width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Returns the level height
     * @return level height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Returns the array of fixed tile locations
     * @return the fixed tile locations array
     */
    public Point[] getFixedTileLocations() {
        return fixedTileLocations;
    }

    /**
     * Returns the array of fixed tiles
     * @return the fixed tiles array
     */
    public FloorTile[] getFixedTiles() {
        return fixedTiles;
    }

    /**
     * Returns the tiles of the bags
     * @return the bag tiles array
     */
    public Tile[] getBagTiles() {
        return bagTiles;
    }

    /**
     * Returns the array of the player starting locations
     * @return thje player locations array
     */
    public Point[] getPlayerStartingLocations() {
        return playerStartingLocations;
    }

    /**
     * Create and return an arraylist of all the levels in the levels folder, instatiated
     * @return arraylist of all levels
     */
    public static ArrayList<Level> getAllLevels() {
        ArrayList<Level> levels = new ArrayList<Level>();
        //Creating a File object for directory
        File directoryPath = new File("levels/");
        //List of all files and directories
        File filesList[] = directoryPath.listFiles();
        for(File file : filesList) {
            levels.add(
                new Level(FileIntegration.read(file.getAbsolutePath()), file.getName().substring(0, file.getName().lastIndexOf('.')))
            );
        }
        return levels;
    }

}