import java.util.ArrayList;
import java.io.File;
import java.io.Serializable;

/**
 * PlayerProfile.java
 * A class designed to create and load player profiles
 * @author Meghan Del Rosario
 * @author Abby Stevenson
 * Last edited 20/11
 */
public class PlayerProfile implements Serializable {

    private String profileName;
    private int wins;
    private int losses;

    /**
     * Loops through arraylist of stored names and checks if the new profile name exists
     * If the profile name exists, throw exception
     * Else create a new profile
     * @param profileName - The player's name
     */
    public PlayerProfile(String profileName) {
        ArrayList<PlayerProfile> players = playerList();
        try {
            for (int i = 0; i < players.size(); i++) {
                if (players.get(i).getProfileName().equals(profileName)) {
                    throw new Exception("Profile already exists");
                }
            }
            this.profileName = profileName;
            this.wins = 0;
            this.losses = 0;
            save();
        } catch (Exception e) {
            e.printStackTrace();
        }
             
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileName() {
        return profileName;
    }

    public int getWins() {
        return wins;
    }

    public void incWins(){
        this.wins++;
    }

    public int getLosses() {
        return losses;
    }

    public void incLosses(){
        this.losses++;
    }

    /**
     * Saves the profile to file
     */
    public void save() {
        FileIntegration.serialise(this, "profiles/"+this.getProfileName());
    }

    /**
     * Loads the profile
     * @param name - Name of player
     * @return FileIntegration.deserialise("profiles/" + name) - deserialization of object
     */
    public static PlayerProfile load(String name) {
        return FileIntegration.deserialise("profiles/" + name);
    }

    /**
     * Gets all of the player profiles on file
     * @return players - ArrayList of current players
     */
    public static ArrayList<PlayerProfile> playerList() {
        return FileIntegration.getAll("profiles/");
    }

    /**
     * get the total games that the player has ever played
     * @return int, their wins added to their losses
     */
    public int getTotalGames(){
        return (this.wins + this.losses);
    }

}