/**
 * ActionTile.java
 * This acts as a parent to the board action tiles and player action tiles,
 * they have in common player which is stored here.
 * This player that is stored is the player that currently holds the tile.
 * 
 * @author Dmitriy
 * @version 1.3
 * Updated 02/12/2020
 * Updated by Charlie Bramble
 */
public class ActionTile extends Tile{

    //The player currently holding the tile
    private Player player;
    
    /**
     * Constructor for an action tile
     * @param tileType TileType enum, To signify what tile type the tile is
     */
    public ActionTile(TileType tileType){
    	super(tileType);
    }

    /**
     * Set the current player that holds this tile in their inventory
     * @param player Player, the player object of the player holding this
     */
    public void setPlayer(Player player){
    	this.player = player;
    }

    /**
     * Get the current player that holds this tile
     * @return Player this.player
     */
    public Player getPlayer() {
        return this.player;
    }
}