import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
/**
 * Board.java
 * The actual game board container that contains everything that is in play
 * 
 * @author Meghan Del Rosario
 * @author Abby Stevenson
 * Updated 02/12/2020
 * Updated by Charlie Bramble
 * @version 1.1
 */
public class Board implements Serializable {

	//The 2d array that contains all of the game pieces
    private FloorTile[][] gameBoard;
    private Game game;
    private Level currentLevel;

	/**
	 * The constructor used to create a new game board
	 * @param levelInfo Level, the level file that contains the board specifications
	 * @param game Game, the game that the board is contained within
	 */
    public Board (Level levelInfo, Game game) {
		this.currentLevel = levelInfo;
		this.game = game;
        this.gameBoard = new FloorTile[levelInfo.getWidth()][levelInfo.getHeight()];
        addFixedTiles(levelInfo.getFixedTiles(), levelInfo.getFixedTileLocations());
        //setRowColumnForFixed(levelInfo.getFixedTiles(), levelInfo.getFixedTileLocations()); EITHER FIX OR REMOVE TY
        populateBoard(levelInfo.getFixedTileLocations());
    }

	/**
	 * Inserting a tile into the game board
	 * @param floorTile FloorTile, the floor tile that is being inserted into the board
	 * @param push Direction enum, the direction that the player is pushing the piece
	 * @param layer int, the location that the player is attempting to push the piece
	 * @return returnTile FloorTile, the tile that falls off the board after the push
	 */
    public FloorTile insertTile (FloorTile floorTile, Direction push, int layer) {
		FloorTile returnTile;
		if (push == Direction.RIGHT) {
    		returnTile = this.gameBoard[this.gameBoard.length][layer];
    		shiftX(0, this.gameBoard.length, layer);
    		this.gameBoard[0][layer] = floorTile;
    		return returnTile;
    	} else if (push == Direction.LEFT){
    		returnTile = this.gameBoard[0][layer];
    		shiftX(this.gameBoard.length, 0, layer);
    		this.gameBoard[this.gameBoard.length][layer] = floorTile;
    		return returnTile;
    	} else if (push == Direction.DOWN) {
    		returnTile = this.gameBoard[layer][this.gameBoard[0].length];
    		shiftY(0,this.gameBoard[0].length, layer);
    		this.gameBoard[layer][0] = floorTile;
    		return returnTile;
    	} else {
    		returnTile = this.gameBoard[layer][0];
    		shiftY(this.gameBoard[0].length, 0, layer);
    		this.gameBoard[layer][this.gameBoard[0].length] = floorTile;
    		return returnTile;
    	}
	}
	public FloorTile getTileFromIndex(int x, int y){
    	return gameBoard[x][y];
	}

	/** =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
	 * Shifting the gameboard peices in the x axis that are required to be shifted
	 * @param start 
	 * @param end
	 * @param y
	 */
    private void shiftX(int start, int end, int y) {
    	if (end > start) {
    		for (int i = end; i > start; i--) {
    			this.gameBoard[i][y] = this.gameBoard[i-1][y];
    		}
    	} else {
    		for (int i = end; i < start; i++) {
    			this.gameBoard[i][y] = this.gameBoard[i+1][y];
    		}
    	}
    }
	
	/** =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
	 * Shifting the gameboard peices in the y axis that are required to be shifted
	 * @param start
	 * @param end
	 * @param x
	 */
    private void shiftY(int start, int end, int x) {
    	if (end > start) {
    		for (int i = end; i > start; i--) {
    			this.gameBoard[x][i] = this.gameBoard[x][i-1];
    		}
    	} else {
    		for (int i = end; i < start; i++) {
    			this.gameBoard[x][i] = this.gameBoard[x][i+1];
    		}
    	}
    }

	/** ==============================================NEEDS IMPLEMENTING===============================================================
	 * Selecting a player on the game board
	 * @param x
	 * @param y
	 * @return
	 */
    public Player choosePlayer (int x, int y) {
    	return null;
    }

	/**
	 * Checks a specific board position to see whether the player can move there
	 * @param here Point, the location they are moving from
	 * @param there Point, the location they are moving to
	 * @return Boolean, whether they can move their successfully.
	 */
    public boolean canMove (Point here, Point there) {
		int hereX = 	(int) here.getX();
		int hereY = 	(int) here.getY();
		int thereX = 	(int) there.getX();
		int thereY = 	(int) there.getY();

		if (thereX - hereX == 0){
			//MOVE LEFT OR RIGHT
			if (thereY - hereY == 1){
				//MOVE RIGHT
				if (this.gameBoard[hereX][hereY].edgeOpen(1) && this.gameBoard[thereX][thereY].edgeOpen(3)){
					return true;
				} else {
					return false;
				}
			} else if (thereY - hereY == -1) {
				//MOVE LEFT
				if (this.gameBoard[hereX][hereY].edgeOpen(3) && this.gameBoard[thereX][thereY].edgeOpen(1)){
					return true;
				} else {
					return false;
				}
			}
			// MOVE UP OR DOWN
		} else if (thereX - hereX == -1) {
			//MOVE UP
			if(this.gameBoard[hereX][hereY].edgeOpen(2) && this.gameBoard[thereX][thereY].edgeOpen(0)){
				return true;
			} else {
				return false;
			}

		} else if (thereX - hereX == 1) {
			//MOVE DOWN
			if(this.gameBoard[hereX][hereY].edgeOpen(0) && this.gameBoard[thereX][thereY].edgeOpen(2)){
				return true;
			} else {
				return false;
			}
		}
		return false;
    }

	/**
	 * Changes the states of tiles, (fire or ice). If it is an invalid position then an exception is thrown.
	 * @param coordinate the centre position the action tile is to be used.
	 * @param tileType the board action tile type being used.
	 * @return an array list of the tiles that were affected when using applying the area effect.
	 * @throws Exception if a fire tile was used but there is a player on one of the tiles.
	 */
    public ArrayList<FloorTile> applyAreaEffect (Point coordinate, TileType tileType) throws Exception {
    	ArrayList<Point> tilePos = checkSpaces(coordinate, tileType);
    	ArrayList<FloorTile> affectedTiles = new ArrayList<FloorTile>();
    	if (tilePos.isEmpty()) {
    		throw new Exception("There is a player on one of the tiles");
		} else {
    		for (Point pos: tilePos) {
    			if (tileType == TileType.HORDE && !this.gameBoard[pos.x][pos.y].isBurning()) {
					this.gameBoard[pos.x][pos.y].toggleFire();
					affectedTiles.add(this.gameBoard[pos.x][pos.y]);
				} else if (tileType == TileType.ICE && !this.gameBoard[pos.x][pos.y].isFrozen()) {
					this.gameBoard[pos.x][pos.y].toggleFreeze();
					affectedTiles.add(this.gameBoard[pos.x][pos.y]);
				}
			}
		}
    	return affectedTiles;
    }

	/**
	 * Checks for which co-ordinates in a 3x3 area are within the board.
	 * In the case of a fire tile, checks if a player is on any of the tiles
	 * @param coordinate the centre point of which the action tile wants to be activated.
	 * @param tileType which action tile wants to be activated.
	 * @return an arrayList of points, which are the positions of tiles to be effected.
	 * if action tile is fire and there is a player on one of the tiles the array will be empty.
	 */
    private ArrayList<Point> checkSpaces(Point coordinate, TileType tileType) {
		ArrayList<Point> tiles = new ArrayList<Point>();
		for (int i = coordinate.x - 1; i <= coordinate.x + 1; i ++) {
			for (int j = coordinate.y - 1; j <= (int) coordinate.y + 1; j ++) {
				if (i >= 0 && i < this.gameBoard.length && j >= 0 && j < this.gameBoard[0].length) {
					if (tileType == TileType.HORDE && playerPresent(new Point(i,j))) {
						tiles = new ArrayList<Point>(); //empties array
						i += 3;		//forces out of loops because a player is present
						j += 3;
					} else {
						tiles.add(new Point(i,j));
					}
				}
			}
		}
		return tiles;
	}

	/**
	 * Checks is a player is in a given position.
	 * @param pos the position to be checked.
	 * @return true if a player is present, false is not.
	 */
	private Boolean playerPresent(Point pos) {
    	Player[] players = this.game.getPlayers();
    	Boolean present = false;
    	for (Player p: players) {
    		if (p.requestLocation() == pos) {
    			present = true;
			}
		}
    	return present;
	}

	/**
	 * Returns the game that this board is contained with in
	 * @return Game game, the game
	 */
    public Game getGame () {
        return this.game;
    }
	
	/**
	 * Adding in the fixed tile locations onto the board
	 * @param tiles FloorTile[], an array of the floor tile objects to be made fixed
	 * @param positions Point[], an array of all the points locations that will be fixed
	 */
    private void addFixedTiles(FloorTile[] tiles, Point[] positions) {
    	for (int i = 0; i < tiles.length; i++) {
    		this.gameBoard[(int) positions[i].getX()][(int) positions[i].getY()] = tiles[i];
    	}
	}

	/**
	 * Populating the board with tiles but avoiding the fixed tile locations.
	 * @param fixedTiles Point[], the fixed tile locations that shouldnt be altered
	 */
    private void populateBoard(Point[] fixedTiles) {
    	for (int x = 0; x < this.gameBoard.length; x++) {
			System.out.println("X is: " +x);
    		for (int y = 0; y < this.gameBoard[x].length; y++) {
				System.out.println("Y is: " +y);
    			if (isCellEmpty(x,y)) {
    				Boolean success = false;
    				while (!success) {
    					Tile tile = this.game.getBag().take();
    					TileType t = tile.getTileType();
    					if (t == TileType.STRAIGHT || t == TileType.CORNER
    											   || t == TileType.TSHAPED) {
    						this.gameBoard[x][y] = randRotate((FloorTile) tile);
    						success = true;
    					} else {
    						this.game.getBag().insertNoRand(tile);
    					}
    				}
    			}
    		}
    	}
    	this.game.getBag().randomise();
    }
	
	/** =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
	 * Method to fix tiles at X and Y positions an edgeopener is required for the method
	 * @param tiles
	 * @param positions
	 */
	// private void setRowColumnForFixed(FloorTile[] tiles, Point[] positions)
	// {
	// 	//Setting the coordinates of the edge tiles as fixed for position i
	// 	for (int i = 0; i < tiles.length; i++) {
	// 		this.gameBoard[0][(int) positions[i].getY()].isFixed();
	// 		this.gameBoard[(int) positions[i].getX()][0].isFixed();
	// 		this.gameBoard[tiles.length - 1][(int) positions[i].getY()].isFixed();
	// 		this.gameBoard[(int) positions[i].getX()][tiles.length - 1].isFixed();		
	// 	}
	// }

	/** =============================================SOMEONE FILL THIS OUT FOR ME=================================================================
	 * Checks to see if a location is empty
	 */
    private Boolean isCellEmpty(int x, int y) {
    	return null == this.gameBoard[x][y];
    }
	
	/**=============================================SOMEONE FILL THIS OUT FOR ME=================================================================
	 * 
	 * @param f
	 * @return
	 */
    private FloorTile randRotate(FloorTile f) {
    	Random rand = new Random();
    	for (int i = 0; i <= rand.nextInt(4); i++) {
    		f.rotateL();
    	}
    	return f;
	}

	/**
	 * Get the entire 2d array that contains all of the tiles on the board
	 * @return FloorTile[][], the game board
	 */
	public FloorTile[][] getGameBoard() {
		return this.gameBoard;
	}

	/**
	 * Get the level details
	 * @return Level, the level details
	 */
	public Level getCurrentLevel() {
		return this.currentLevel;
	}
}