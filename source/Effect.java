import java.awt.Point;

/**
 * Effect.java
 * @version 1.0
 * 
 * Created - 13/11/2020
 * Last Editor - Charlie Bramble
 * Edited - 02/12/2020
 * @author Tyler Berry
 * @author Ethio Mascarenhas
 * 
 * A class designed to store the effects of the game
 */

public class Effect 
{
   
    private Point location;
    private TileType affectType;
    private int turnCount;

    /**
     * Constructor for the effect object
     * @param location Point, the location that the effect took place
     * @param affectType TileType enum, the type that the affect is
     * @param turnCount int, the remaining turns of the effect
     */
    public Effect (Point location, TileType affectType, int turnCount) {
        this.location = location;
        this.affectType = affectType;
        this.turnCount = turnCount;
    }

    /**
     * Decrement the turn counter by 1
     */
    public void decrementCount()
    {
    	this.turnCount -= 1;
    }

    /**
     * Return the turn counter
     * @return int, the turn counter
     */
    public int getCount()
    {
    	return this.turnCount;
    }

    /**
     * get the x of the location of the effect
     * @return int, the x location of the effect
     */
    public int getX()
    {
    	return (int) this.location.getX();
    }

    /**
     * get the y of the location of the effect
     * @return int, the y location of the effect
     */
    public int getY()
    {
    	return (int) this.location.getY();
    }

    /**
     * Get the tile type of the effect
     * @return TileType, the tile type of the effect
     */
    public TileType getType()
    {
    	return this.affectType;
    }
}