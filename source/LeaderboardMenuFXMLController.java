/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


/**
 *
 * @author Harry
 */
public class LeaderboardMenuFXMLController extends SwanzGUI {

    private ArrayList<PlayerProfile> playerList = PlayerProfile.playerList();

    @FXML
    private VBox leaderboard = new VBox();


    @FXML
    private void backButtonHandler(ActionEvent event) throws Exception {
        mainMenu();
    };
    public void initialize() {

        leaderboard.setAlignment(Pos.CENTER);
        playerList.sort(Comparator.comparing(PlayerProfile::getWins).reversed());

        for(int i = 0; i < playerList.size(); i++) {
            Label label = new Label();
            label.setStyle("-fx-font-size : 24;");
            label.setLineSpacing(10.0);
            label.setText(playerList.get(i).getProfileName() + "  wins: " + playerList.get(i).getWins() + "  losses: " + playerList.get(i).getLosses());
            leaderboard.getChildren().add(label);

        }
    }

}
