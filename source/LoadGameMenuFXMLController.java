/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;


/**
 *
 * @author Harry
 */
public class LoadGameMenuFXMLController extends SwanzGUI {
    @FXML
    private ChoiceBox<String> gameDropDown = new ChoiceBox<>();

    @FXML
    Button loadButton = new Button();

    private String selected;
    private ArrayList<Game> gameList;
    private Game toLoad;





    @FXML
    private void backButtonHandler(ActionEvent event) throws Exception {
        mainMenu();
    };

    @FXML
    private void loadButtonHandler(ActionEvent event) throws Exception {
        selected = gameDropDown.getValue();
        System.out.println(selected);
        setGameInstance(Game.loadGame(selected));
        gameStage();

    };

    //@FXML
   // private void gameDropDownHandler() throws Exception {
   //     selected = gameDropDown.getValue().toString();
   //     for (int i = 0; i < gameList.size(); i++) {
   //         if (gameList.get(i).getGameName().equals(selected)){
   //            this.toLoad = gameList.get(i);
   //            System.out.println("works");
   //         }
  //      }
 //   };
    @FXML
    public void initialize() {
        ArrayList<Game> gameList = Game.getAllGames();
        for (int i = 0; i < gameList.size(); i++) {
            gameDropDown.getItems().add(gameList.get(i).getGameName());
        }

    }

}



