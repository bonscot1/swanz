import java.io.Serializable;

/**
 * Tile.java The absolute parent type of all tiles
 * 
 * @author Charlie Bramble
 * Edited on 02/12/2020
 * Edited 04/12/2020 by Dmitriy
 * @version 1.3
 */

public class Tile implements Serializable {

    private TileType tileType;

    /**
     * The new Tile object constructor
     * @param tileType TileType, the type of the tile
     */
    public Tile(TileType tileType){
        this.tileType = tileType;
    }

    public boolean isActionTile() {
        switch (this.getTileType()) {
            case ICE:
            case HORDE:
            case DOUBLEMOVE:
            case BACKTRACK:
                return true;
            default:
                return false;
        }
    }

    public TileType getTileType(){
        return this.tileType;
    }
    
}