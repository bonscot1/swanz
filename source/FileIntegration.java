/**
 * FileIntegration.java 
 * A class designed to handle most retrieval and writing to file required
 * its main purpose is to hash and salt and files before storage and vise versa for retrieval
 * Published - 08/11/2020
 * Last Editor - Charles Bramble
 * Edited - 22/11/2020
 * @author Charles Bramble
 * @version 1.1
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException; // Needed for the file not found error that may occur
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner; //Scanner class to read files


public class FileIntegration {

    /**
     * Goes to a file and reads the contents then returns it
     * @param fileLocation String - the location of the file
     * @return String fileContent - the content of the file
     */
    public static Scanner read(String fileLocation) {
        Scanner fileScan = null;
        try {
            File fileRead = new File(fileLocation);
            fileScan = new Scanner(fileRead);
          } catch (FileNotFoundException e) {
            System.out.println("An issue occured finding that file! \n");
            e.printStackTrace();
          }
        return fileScan;
    } 

    /**
     * Generic way to serialise a class
     * 
     * @param <T>          The class type that will be passed and serialised
     * @param chosenClass  The actual class that will be passed
     * @param fileLocation The location + file name of the file to be written
     * @return
     */
    public static <T> void serialise(T chosenClass, String fileLocation) {
        try {
            FileOutputStream fileWrite = new FileOutputStream(fileLocation);
            ObjectOutputStream objectOut = new ObjectOutputStream(fileWrite);
            objectOut.writeObject(chosenClass);
            objectOut.close();
            fileWrite.close();
        } catch (IOException e) {
            System.out.println("Issue with serialising class");
            e.printStackTrace();
        }
    } 

    /**
     * Generic way to deserialise a class
     * @param <T> The class type that should be returned for the process
     * @param fileLocation The location of the file to be deserialised to be a class
     */
    public static <T> T deserialise(String fileLocation) {
        FileInputStream fileRead = null;
        ObjectInputStream objectIn = null;
        T readObject = null;
        try {    
            fileRead = new FileInputStream(fileLocation);
            objectIn = new ObjectInputStream(fileRead);
            readObject = (T) objectIn.readObject();
            objectIn.close(); 
            fileRead.close(); 
        } catch(IOException e) { 
            System.out.println("Issue deserialising this object file"); 
            e.printStackTrace();
        } catch(ClassNotFoundException e) { 
            System.out.println("This class type cannot be found"); 
            e.printStackTrace();
        } 
        return readObject;
    } 

    /**
     * Loops through all files in a location and deserialises them
     * then puts them into an array list and return them to the user
     * @param <T> The generic class type that is in the array list
     * @param location String, the location of the files
     * @return ArrayList<T>, all of the deserialised files
     */
    public static <T> ArrayList<T> getAll(String location) {
        ArrayList<T> all = new ArrayList<T>();
        File directoryPath = new File(location);
        File filesList[] = directoryPath.listFiles();
        for(File file : filesList) {
            all.add(FileIntegration.deserialise(file.getAbsolutePath()));
        }
        return all;
    }

}