
/**
 * Player.java 
 * A class designed to store the details of the player such as 
 * profile info, inventory and their last known position
 * 
 * Created - 13/11/2020
 * Last Editor - Charlie Bramble
 * Edited - 02/12/2020
 * @author Tyler Berry
 * @author Ethio Mascarenhas
 * @version 1.3
 * 
 */

import java.util.ArrayList;
import java.awt.Point;
import java.io.Serializable;

/**
 * Player.java 
 * A class designed to store the details of the player such as 
 * profile info, inventory and their last known position
 *
 * Created - 13/11/2020
 * Last Editor - Charlie Bramble
 * Edited - 02/12/2020
 * Edited 04/12/2020 by Dmitriy
 * @author Tyler Berry
 * @author Ethio Mascarenhas
 * @version 1.4
 */
public class Player implements Serializable {

    private PlayerProfile profile;
    private ArrayList<ActionTile> inventory;
    private Point playerPosition;
    private Game game;
    private ArrayList<Point> previousPositions;
    private ArrayList<Effect> previousActions;

    /**
     * A new player constructor
     * @param profile PlayerProfile, the player's profile
     * @param game Game, the game the player is in
     */
    public Player(PlayerProfile profile, Game game) {
    	this.profile = profile;
    	this.game = game;
    }
    
    /**=============================================SOMEONE FILL THIS OUT / implement FOR ME=================================================================
     * A players turn shall go here
     */
    public void turn() {
        Tile newTile = getGame().getBag().take();
        if (newTile.isActionTile()) {
            ActionTile actionTile = (ActionTile) newTile;
            inventory.add(actionTile);
        } else {
            FloorTile floorTile = (FloorTile) newTile;
            floorTile.rotateL();//number of rotations = number of clicks from GUI of the board
            //getGame().getBoard().
        }
    }

    /**
     * Move the player's location on the game board
     * @param xy Point, the new location of the player
     */
    public void move(Point xy) {
        setX(xy.getX());
        setY(xy.getY());
    }

    /**
     * Set the x location of the player
     * @param x Double, the x axis location of the player
     */
    private void setX(Double x) {
        playerPosition.setLocation(x, playerPosition.getY());
    }

    /**
     * Set the y location of the player
     * @param y Double, the y axis location of the player
     */
    private void setY(Double y) {
        playerPosition.setLocation(playerPosition.getX(), y);
    }

    public int getX() {
    	return (int) this.playerPosition.getX();
    }

    public int getY() {
    	return (int) this.playerPosition.getY();
    }
    
    public Game getGame() {
    	return this.game;
    }
    
    public ArrayList<Point> getPreviousPositions() {
    	return this.previousPositions;
    }
    
    public ArrayList<Effect> getPreviousActions() {
    	return this.previousActions;
    }

    public String getProfileName() {
        return this.profile.getProfileName();
    }

    public ArrayList<ActionTile> getInventory() {
        return inventory;
    }

    public void useAction() {
    	
    }
    //=============================================SOMEONE FILL THIS OUT/ Implement FOR ME=================================================================
    // public ActionTile requestActionTile() {	
    // }
    
    public Point requestLocation() {
    	return this.playerPosition;
    }
}
