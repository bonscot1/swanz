/**
 * SwanzGUI.java
 * @version 1.2
 * @author Harry Stadling, Sam Harry
 * last edited 21/11
 * Class to create the Stage and display the splash screen upon opening the game,
 */
import javafx.application.Application;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.nio.file.Paths;


public class SwanzGUI extends Application{

	private static Game gameInstance;
    private static Stage mainStage;
	public MediaPlayer mediaPlayer;
    
    /** 
     * method called when annother class needs to update the Scene,
     * @return  the mainStage to update the scene,
     */
    public static Stage getStage() {

        return mainStage;
    }

	/**
     * main, runs start,
     * @param args
     */
	public static void main(String[] args) { 
	    launch(args);

    }
    
	public SwanzGUI() {
		//not done,
	}

	/**
	 * Method to display the splash screen then to instantiate the Menu Class,
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		music();
        mainStage = primaryStage;
        mainStage.setTitle("SwanZ");
        Parent splashLayout = FXMLLoader.load(getClass().getResource("SplashFXML.fxml"));

		//create the splash scene,
		Scene splashScene = new Scene(splashLayout);
		mainStage.setScene(splashScene);
        mainStage.show();

    }
	public void mainMenu() throws Exception{

		Parent mainMenuLayout = FXMLLoader.load(getClass().getResource("MainMenuFXML.fxml"));
		//create a scene
		Scene mainMenuScene = new Scene(mainMenuLayout);
		mainStage.setScene(mainMenuScene);

	}
	public void newGameMenu() throws Exception{

		Parent newGameMenuLayout = FXMLLoader.load(getClass().getResource("NewGameMenuFXML.fxml"));
		//create a scene
		Scene newGameMenuScene = new Scene(newGameMenuLayout);
		mainStage.setScene(newGameMenuScene);

	}
	public void loadGameMenu() throws Exception{

		Parent loadGameMenuLayout = FXMLLoader.load(getClass().getResource("LoadGameMenuFXML.fxml"));
		//create a scene
		Scene loadGameMenuScene = new Scene(loadGameMenuLayout);
		mainStage.setScene(loadGameMenuScene);

	}
	public void leaderboardMenu() throws Exception{

		Parent leaderboardMenuLayout = FXMLLoader.load(getClass().getResource("LeaderboardMenuFXML.fxml"));
		//create a scene
		Scene leaderboardMenuScene = new Scene(leaderboardMenuLayout);
		mainStage.setScene(leaderboardMenuScene);

	}
	public void optionsMenu() throws Exception{

		Parent optionsMenuLayout = FXMLLoader.load(getClass().getResource("OptionsMenuFXML.fxml"));
		//create a scene
		Scene optionsMenuScene = new Scene(optionsMenuLayout);
		mainStage.setScene(optionsMenuScene);

	}
	public void gameStage() throws Exception{

		Parent gameStageLayout = FXMLLoader.load(getClass().getResource("GameStageFXML.fxml"));
		//create a scene
		Scene gameStageScene = new Scene(gameStageLayout);

		mainStage.setScene(gameStageScene);
		mainStage.setMaximized(true);

	}
	public void setGameInstance(Game game){
		this.gameInstance = game;
	}

	//plays music using the media player
	public void music() {
		String s = "media/audio/swanz theme.wav";
		Media h = new Media(Paths.get(s).toUri().toString());
		mediaPlayer = new MediaPlayer(h);
		mediaPlayer.setOnEndOfMedia(new Runnable() {
			public void run() {
				mediaPlayer.seek(Duration.ZERO);
			}
		});
		mediaPlayer.setOnReady(new Runnable() {
			@Override
			public void run() {
				mediaPlayer.play();
			}
		});
	}

	public static Game getGameInstance(){
		return gameInstance;
	}

}