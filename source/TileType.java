/**
 * TileType.java
 * All of the different types that tiles can be
 * @author Dmitriy
 * @version 1.2
 * Updated by Charlie Bramble
 * Edited 02/12/2020
 * 
 */
public enum TileType {
	STRAIGHT,
	CORNER,
	TSHAPED,
	GOAL,
	ICE,
	HORDE,
	DOUBLEMOVE,
	BACKTRACK
}
