/**
 * Direction.java
 * All of the possible direction values on the board
 * @author Charlie Bramble
 * @version 1.1
 * Updated by Charlie Bramble
 * Edited 02/12/2020
 * 
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT
}
